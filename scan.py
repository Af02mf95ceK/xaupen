import os
import cowsay
import errno

class Scan:
    target_ip = "127.0.0.1"

    def __init__(self, target_ip):
        self.target_ip = target_ip

    def nmap_scan(self):
        cowsay.cow("Scanning with Nmap")
        output = os.popen("nmap " + self.target_ip).read()
        Writter().write_in_file("nmap",output)
        cowsay.cow("Scanning with Nmap finished")

    def gobuster_scan(self):
        cowsay.cow("Scanning"+ self.target_ip +" with Gobuster ")
        output = os.popen("gobuster -u " + self.target_ip + "-w assets/wordlist/big.txt").read()
        Writter().write_in_file("gobuster",output)
        print("This is an example por PR")
        cowsay.cow("Scanning with Nmap Gobuster")

class Writter:
    output_directory = "reports"

    def __init__(self):
        os.makedirs(self.output_directory, exist_ok=True)

    def write_in_file(self,name,content):
        file = open(self.output_directory+"/"+name+".txt", "w")
        file.write(content)
        file.close()
